# Doczilla Tabs Plugin

This package is a [markdown-it](https://github.com/markdown-it/markdown-it) plugin that supplies tabs (mainly for code type variations) for your [Vitepress](https://vitepress.dev) markdown files.

## Installation

Doczilla tabs requires:

- vitepress

```js
npm install vitepress @zenphp/doczilla-tabs
```

Add `doczilla-tabs` to your `.vitepress/config.{ts,js}` file:

```js
import { tabsMarkdownPlugin } from '@zenphp/doczilla-tabs';
```

Add the plugin to your `markdown` config.

```js
markdown: {
  config(md) {
    md.use(tabsMarkdownPlugin)
  }
},
```

Enhance your theme with the plugin in `.vitepress/theme/theme.{ts,js}`:

```js
import { Theme } from 'your-theme-of-choice';
import { enhanceAppWithTabs } from '@jetstreamlabs/vitepress-plugin-tabs/client';

export default {
  ...Theme,
  enhanceApp({ app }) {
    enhanceAppWithTabs(app);
  },
};
```

### Doczilla Theme

If you're using our [@zenphp/doczilla](https://www.npmjs.com/package/@zenphp/doczilla) theme which is Tailwind based and has no default Vitepress css properties, you'll need create a `.css` file and import that into your theme config.

```css
/* .vitepress/theme/tabs.css */

:root {
  --vp-c-text-2: rgba(235, 235, 245, 0.6);
  --vp-c-text-1: rgba(255, 255, 245, 0.86);
  /* match shiki light theme */
  --vp-c-bg-soft: #222436;
  --vp-c-divider: rgba(82, 82, 89, 0.32);
  /* match theme primary color */
  --vp-c-brand: theme('colors.primary.500');
}

html.dark {
  /* match shiki dark theme */
  --vp-c-bg-soft: #020617;
}

.plugin-tabs--content > div[class*='language-'] {
  @apply m-0;
}

.plugin-tabs--content > div[class*='language-'] pre {
  @apply p-0 mt-0 !important;
}
```

Then import the css into `theme.{ts,js}`

```js
import { Theme } from 'your-theme-of-choice';
import { enhanceAppWithTabs } from '@jetstreamlabs/vitepress-plugin-tabs/client';

import './tabs.css';

export default {
  ...Theme,
  enhanceApp({ app }) {
    enhanceAppWithTabs(app);
  },
};
```

That should be it.

## Usage

Open tabs with `:::=tabs` and close with `:::`

Each tab is self closing and requires only `::` and the name to display on the tab without spaces.

```markdown
:::=tabs

::first tab
// some content

::second tab
// other content

:::
```

Enjoy

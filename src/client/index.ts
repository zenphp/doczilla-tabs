import type { App } from 'vue'
import PluginTabs from './PluginTabs.vue'

export const enhanceAppWithTabs = (app: App) => {
  app.component('PluginTabs', PluginTabs)
}

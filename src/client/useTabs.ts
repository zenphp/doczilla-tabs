import { ref, computed } from 'vue'
import type { Ref } from 'vue'

export const useTabs = <T extends string>(acceptValues: Ref<T[]>) => {
  const sharedState = ref<T | undefined>()

  const selected = computed({
    get() {
      const acceptVals = acceptValues.value
      const sharedStateVal = sharedState.value

      if (sharedStateVal) {
        return sharedStateVal
      }

      return acceptVals[0]
    },
    set(v) {
      sharedState.value = v
    }
  })

  const select = (newValue: T) => {
    selected.value = newValue
  }

  return { selected, select }
}
